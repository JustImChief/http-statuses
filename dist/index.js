"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "CodeStatuses", {
  enumerable: true,
  get: function get() {
    return _CodeStatuses.default;
  }
});
Object.defineProperty(exports, "Phrases", {
  enumerable: true,
  get: function get() {
    return _Phrases.default;
  }
});
Object.defineProperty(exports, "StatusCodes", {
  enumerable: true,
  get: function get() {
    return _StatusCodes.default;
  }
});
exports.default = void 0;

var _CodeStatuses = _interopRequireDefault(require("./CodeStatuses"));

var _Phrases = _interopRequireDefault(require("./Phrases"));

var _StatusCodes = _interopRequireDefault(require("./StatusCodes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @param {number|string} status
 * @returns {string}
 * @throws {Error}
 */
function getReasonMessage(status) {
  if (_CodeStatuses.default.hasOwnProperty(status.toString())) {
    return _Phrases.default[status];
  }

  if (_StatusCodes.default.hasOwnProperty(status)) {
    return _Phrases.default[_StatusCodes.default[status]];
  }

  throw new Error("Status code does not exist: ".concat(status));
}

var _default = getReasonMessage;
exports.default = _default;